---
author: Albert Ruszkowicz
title: Korona Ziemi
subtitle: Najwyższe szczyty kontynentów
date: 
theme: Warsaw
output: beamer_presentation
header-includes: 
    \usepackage{xcolor}
    \usepackage{caption}
    \captionsetup[figure]{name=Rys}
---  


## Slajd nr 1  

\begin{block}{Korona Ziemi}
Najwyższe szczyty poszczególnych części świata
\end{block}  

\begin{alertblock}{Uwaga:}
Istnieją 4 wersje Korony Ziemi, gdyż niektórzy zaliczają \textcolor{blue}{Elbrus} do szczytu europejskiego, a \textcolor{blue}{Australię} zastępują \textcolor{blue}{Australią i Oceanią}
\end{alertblock}  

## Slajd nr 2 Lista szczytów  

* Azja: \textcolor{red}{Mount Everest} (*8848 m n.p.m.*)
* Ameryka Południowa: \textcolor{red}{Aconcagua} (*6961 m n.p.m.*)
* Ameryka Północna: \textcolor{red}{Denali (McKinley)} (*6190 m n.p.m.*)
* Afryka: \textcolor{red}{Kilimandżaro} (*5895 m n.p.m.*)
* Europa:
    * (według Messnera, Bassa i środowiska wspinaczkowego) \textcolor{red}{Elbrus} (*5642 m n.p.m.*)
    * (według niektórych naukowców) \textcolor{red}{Mont Blanc} (*4810 m n.p.m.*)
* Australia lub Australia i Oceania:
    * (według Bassa) Australia: \textcolor{red}{Góra Kościuszki} (*2230 m n.p.m.*)
    * (według Messnera) Australia i Oceania: \textcolor{red}{Puncak Jaya} (*4884 m n.p.m.*)
* Antarktyda: \textcolor{red}{Masyw Vinsona} (*4892 m n.p.m.*)  


## Slajd nr 3 Azja  

![&nbsp; Mount Everest (8848 m n.p.m.)](1.jpg){ height=50% width=50%}  

## Slajd nr 4 Ameryka Południowa  

![&nbsp; Aconcagua (6961 m n.p.m.)](2.JPG){ height=50% width=50%}

## Slajd nr 5 Ameryka Północna  

![&nbsp; Denali (McKinley) (6190 m n.p.m.)](3.jpg){ height=50% width=50%}  

## Slajd nr 6 Afryka  

![&nbsp; Kilimandżaro (5895 m n.p.m.)](4.jpg){ height=50% width=50%}  

## Slajd nr 7 Europa  

![&nbsp; Elbrus (5642 m n.p.m.)](5.jpg){ height=50% width=50%}  

## Slajd nr 8 Europa  

![&nbsp; Mont Blanc (4810 m n.p.m.)](6.JPG){ height=50% width=50%}  

## Slajd nr 9 Australia  

![&nbsp; Góra Kościuszki (2230 m n.p.m.)](7.JPG){ height=50% width=50%}  

## Slajd nr 10 Australia i Oceania  

![&nbsp; Puncak Jaya (4884 m n.p.m.)](8.jpg){ height=50% width=50%}  

## Slajd nr 11 Antarktyda   

![&nbsp; Masyw Vinsona (4892 m n.p.m.)](9.jpg){ height=50% width=50%}  

## Slajd nr 12 Pierwsi zdobywcy Korony Ziemi  

| Nazwisko i imię         | Narodowość          | Rok   |
| ----------------------- |:-------------------:| -----:|
| Richard Bass            | USA                 | 1985  |
| Gerry Roach             | USA                 | 1985  |
| Pat Morrow              | Kanada              | 1986  |
| Gerhard Schmatz         | Niemcy              | 1986  |
| Reinhold Messner        | Włochy              | 1986  |


## Slajd nr 13 Statystyki  

###
Pierwszym, który zdobył Koronę bez wspomagania tlenem z butli był Reinhold Messner  

###
Pierwszą kobietą, która skompletowała całą listę szczytów, była Japonka Junko Tabei (1992)  

###
Pierwszym Polakiem, który zdobył Koronę był Leszek Cichy (1999)


